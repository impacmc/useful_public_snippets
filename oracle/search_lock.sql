SELECT a.session_id,
  a.oracle_username,
  a.os_user_name,
  b.owner "OBJECT OWNER",
  b.object_name,
  b.object_type,
  a.locked_mode
FROM
  (SELECT object_id,
    SESSION_ID,
    ORACLE_USERNAME,
    OS_USER_NAME,
    LOCKED_MODE
  FROM v$locked_object
  ) a,
  (SELECT object_id, owner, object_name,object_type FROM dba_objects
  ) b
WHERE a.object_id=b.object_id
/
