SELECT tablespace_name,
  ROUND(SUM(used)  /1024/1024) USED_MB,
  ROUND((SUM(total)/1024/1024),0) TOTAL_MB,
  ROUND((SUM(total -used)/1024/1024),0) FREE_MB,
  TO_CHAR(100      *SUM(total - used)/SUM(total),'999,99')
  ||'%' Pct_Free,
  TO_CHAR(100*SUM(used)/SUM(total),'999,99')
  ||'%' Pct_Used
FROM
  (SELECT what,
    tablespace_name,
    file_name,
    bytes_used used,
    CASE
      WHEN autoextensible='NO'
      THEN bytes
      WHEN NVL(maxbytes,0) < bytes
      THEN bytes
      ELSE maxbytes
    END total
  FROM
    (SELECT 'tempfile' what,
      f.file_id,
      f.tablespace_name,
      f.file_name,
      f.relative_fno,
      bytes,
      autoextensible,
      maxbytes,
      bytes_used
    FROM V$temp_space_header s ,
      DBA_TEMP_FILES f
    WHERE (f.file_id     =s.file_id
    AND f.tablespace_name=s.tablespace_name
    AND f.relative_fno   =s.relative_fno)
    AND status           ='AVAILABLE'
    UNION ALL
    SELECT 'datafile' what,
      f.file_id,
      f.tablespace_name,
      f.file_name,
      f.relative_fno,
      f.bytes,
      autoextensible,
      maxbytes,
      f.bytes-NVL(s.bytes,0) bytes_used
    FROM DBA_DATA_FILES f ,
      (SELECT file_id,
        tablespace_name,
        relative_fno,
        SUM(bytes) bytes
      FROM DBA_FREE_SPACE
      GROUP BY file_id,
        tablespace_name,
        relative_fno
      ) s
    WHERE (f.file_id     =s.file_id
    AND f.tablespace_name=s.tablespace_name
    AND f.relative_fno   =s.relative_fno)
    AND status           ='AVAILABLE'
    )
  )
GROUP BY tablespace_name
ORDER BY DECODE(tablespace_name,'TEMP','A',tablespace_name);