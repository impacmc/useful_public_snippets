-- check invalid object 
SELECT COUNT(*),owner,object_type, object_name
FROM   all_objects 
WHERE  status !='VALID' 
GROUP BY owner,object_type, object_name ORDER BY 2;
 
-- generate alter compile statements 
SELECT 'alter '||object_type ||' '||owner||'."'||object_name||'" compile;' 
FROM   all_objects 
WHERE  status !='VALID' 
AND    object_type !='SYNONYM' 
ORDER BY owner;
