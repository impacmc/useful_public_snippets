SELECT OBJECT_NAME, OBJECT_TYPE, owner 
         			 FROM sys.all_objects 
         			 WHERE status = 'INVALID';

begin 
  FOR cur IN (SELECT OBJECT_NAME, OBJECT_TYPE, owner 
         			FROM sys.all_objects 
         			WHERE status = 'INVALID' ) LOOP 
      BEGIN
		    if cur.OBJECT_TYPE = 'PACKAGE BODY' then 
		     	EXECUTE IMMEDIATE 'alter ' || cur.OBJECT_TYPE || ' "' ||  cur.owner || '"."' || cur.OBJECT_NAME || '" compile body'; 
		    else 
		     	EXECUTE IMMEDIATE 'alter ' || cur.OBJECT_TYPE || ' "' ||  cur.owner || '"."' || cur.OBJECT_NAME || '" compile'; 
		    end if; 
      EXCEPTION
  			WHEN OTHERS THEN NULL; 
		END;
  end loop; 
end;